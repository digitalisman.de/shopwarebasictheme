{extends file="parent:frontend/checkout/ajax_cart.tpl"}
{* Basket link *}
{block name='frontend_checkout_ajax_cart_button_container_inner'}
	{* Add product using a voucher *}
    {block name='frontend_checkout_cart_footer_add_voucher'}
    		<h5>{s name='VoucherFormHeadline' namespace="frontend/checkout/ajax_cart"}Gutscheincode{/s}</h5>
    
        <form method="post" action="{url action='addVoucher' sTargetAction=$sTargetAction}" class="table--add-voucher add-voucher--form">
            <div class="add-voucher--panel block-group">
                {block name='frontend_checkout_cart_footer_add_voucher_field'}
                    {s name="CheckoutFooterAddVoucherLabelInline" assign="snippetCheckoutFooterAddVoucherLabelInline"}{/s}
                    <input type="text" class="add-voucher--field is--medium block" name="sVoucher" placeholder="{$snippetCheckoutFooterAddVoucherLabelInline|escape}" />
                {/block}

                {block name='frontend_checkout_cart_footer_add_voucher_button'}
                    <button type="submit" class="add-voucher--button is--medium btn is--primary is--center block">
                        <i class="icon--arrow-right"></i>
                    </button>
                {/block}
            </div>
        </form>
    {/block}
	{$smarty.block.parent}
{/block}