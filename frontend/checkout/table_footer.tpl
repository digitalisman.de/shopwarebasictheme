{extends file="parent:frontend/checkout/table_footer.tpl"}

{* Versand Icons *}
{block name="frontend_checkout_footer_text_dispatch"}
    {if $sMenu.shippingicons}
    	<ul class="shipping-icons">
	    	{block name="frontend_index_footer_column_payment_icons_before"}{/block}
				{foreach $sMenu.shippingicons as $item}
					<li><img src="{link file='frontend/_public/src/img/icons/PaymentIcons/'}{$item.description}.png" alt="{$item.description}"></li>
				{/foreach}
			{block name="frontend_index_footer_column_payment_icons_after"}{/block}
    	</ul> 
    {else}
    	<p class="benefit--text">
    	    {s namespace="frontend/checkout/cart" name="CheckoutFooterBenefitTextDispatch"}{/s}
    	</p>
    {/if}
    
{/block}

{* Payment Icons *}
{block name="frontend_checkout_footer_text_payment"}
    {if $sMenu.payment}
    	<ul class="payment-icons">
	    	{block name="frontend_index_footer_column_payment_icons_before"}{/block}
				{foreach $sMenu.payment as $item}
					<li><img src="{link file='frontend/_public/src/img/icons/PaymentIcons/'}{$item.description}.png" alt="{$item.description}"></li>
				{/foreach}
			{block name="frontend_index_footer_column_payment_icons_after"}{/block}
    	</ul>    	
    {else}
    	<p class="benefit--text">
        	{s namespace="frontend/checkout/cart" name="CheckoutFooterBenefitTextPayment"}{/s}
    	</p>	
    {/if}
    
{/block}