{extends file="parent:frontend/detail/content.tpl"}

{block name='frontend_detail_index_image_container'}

	{if $sArticle.instock > 0 and $sArticle.instock <= 5}
		<div class="in--stock--warner">
			{s name='DetailInStockWarnerPrev' namespace="frontend/detail/content"}Nur noch{/s} {$sArticle.instock} {s name='DetailInStockWarnerNext' namespace="frontend/detail/content"}verfügbar{/s}
		</div>
	{/if}
	
	{$smarty.block.parent}
{/block}
