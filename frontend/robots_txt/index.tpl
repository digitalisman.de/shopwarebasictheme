{extends file="parent:frontend/robots_txt/index.tpl"}
{block name="frontend_robots_txt_disallows"}

{$smarty.block.parent}
Disallow: /impressum
Disallow: /datenschutz
Disallow: /agb
Disallow: /widerrufsrecht
Disallow: /text-vorkasse
Disallow: /paypal
Disallow: /dpd
Disallow: /dhl
Disallow: /dhl-express
Disallow: /facebook
Disallow: /twitter
Disallow: /instagram
{/block}