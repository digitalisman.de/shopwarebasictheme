{extends file="parent:frontend/index/header.tpl"}

{* Meta opengraph tags *}
{block name='frontend_index_header_meta_tags_opengraph'}
    <meta property="og:type" content="website" />
    <meta property="og:site_name" content="{{config name=sShopname}|escapeHtml}" />
    {if isset($sCategoryContent.attribute.socialmediatitle)}
		<meta property="og:title" content="{{$sCategoryContent.attribute.socialmediatitle}|escapeHtml}" />
		{else}
		<meta property="og:title" content="{{config name=sShopname}|escapeHtml}" />
	{/if}
	{s name="IndexMetaDescriptionStandard" assign="snippetIndexMetaDescriptionStandard"}{/s}
    {if isset($sCategoryContent.attribute.socialmediadesc)}
		<meta property="og:description" content="{$sCategoryContent.attribute.socialmediadesc|truncate:$SeoDescriptionMaxLength:'…'|escapeHtml}" />
		{else}
		<meta property="og:description" content="{block name='frontend_index_header_meta_description_og'}{$snippetIndexMetaDescriptionStandard|truncate:$SeoDescriptionMaxLength:'…'}{/block}" />
	{/if}
    
    <meta property="og:image" content="{link file=$theme.desktopLogo fullPath}" />

    <meta name="twitter:card" content="website" />
    <meta name="twitter:site" content="{{config name=sShopname}|escapeHtml}" />
    <meta name="twitter:title" content="{{config name=sShopname}|escapeHtml}" />
    {s name="IndexMetaDescriptionStandard" assign="snippetIndexMetaDescriptionStandard"}{/s}
    <meta name="twitter:description" content="{block name='frontend_index_header_meta_description_twitter'}{$snippetIndexMetaDescriptionStandard|truncate:$SeoDescriptionMaxLength:'…'}{/block}" />
    <meta name="twitter:image" content="{link file=$theme.desktopLogo fullPath}" />
{/block}