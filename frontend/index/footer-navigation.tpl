{extends file="parent:frontend/index/footer-navigation.tpl"}

{* Service hotline *}
{block name="frontend_index_footer_column_service_hotline"}{/block}

{* Newsletter ausblenden *}
{block name="frontend_index_footer_column_newsletter"}
	{block name="frontend_index_footer_column_payment_icons"}
		<div class="footer--column column--menu block">
			{block name="frontend_index_footer_column_payment_icons_headline"}
			    <div class="column--headline">{s name="sFooterPaymentHeadline"}Zahlungsinformationen{/s}</div>
			{/block}
			{block name="frontend_index_footer_column_payment_icons_content"}
				<div class="column--content">
					<ul class="payment--list">
						{block name="frontend_index_footer_column_payment_icons_before"}{/block}
							{foreach $sMenu.payment as $item}
								<li><img src="{link file='frontend/_public/src/img/icons/PaymentIcons/'}{$item.description}_70.png" alt=""></li>
							{/foreach}
						{block name="frontend_index_footer_column_payment_icons_after"}{/block}
					</ul>
				</div>	
			{/block}
		</div>
	{/block}
{/block}


{block name="frontend_index_footer_column_service_menu"}
    <div class="footer--column column--menu block">
        {block name="frontend_index_footer_column_service_menu_headline"}
            <div class="column--headline">{s name="sFooterShopNavi1"}{/s}</div>
        {/block}

        {block name="frontend_index_footer_column_service_menu_content"}
            <nav class="column--navigation column--content">
                <ul class="navigation--list" role="menu">
                    {block name="frontend_index_footer_column_service_menu_before"}{/block}
                    
                    {foreach $sMenu.bottom as $item}

                        {block name="frontend_index_footer_column_service_menu_entry"}
                            <li class="navigation--entry" role="menuitem">
                                <a class="navigation--link" href="{if $item.link}{$item.link}{else}{url controller='custom' sCustom=$item.id title=$item.description}{/if}" title="{$item.description|escape}"{if $item.target} target="{$item.target}"{/if}>
                                    {$item.description}
                                </a>

                                {* Sub categories *}
                                {if $item.childrenCount > 0}
                                    <ul class="navigation--list is--level1" role="menu">
                                        {foreach $item.subPages as $subItem}
                                            <li class="navigation--entry" role="menuitem">
                                                <a class="navigation--link" href="{if $subItem.link}{$subItem.link}{else}{url controller='custom' sCustom=$subItem.id title=$subItem.description}{/if}" title="{$subItem.description|escape}"{if $subItem.target} target="{$subItem.target}"{/if}>
                                                    {$subItem.description}
                                                </a>
                                            </li>
                                        {/foreach}
                                    </ul>
                                {/if}
                            </li>
                        {/block}
                    {/foreach}

                    {block name="frontend_index_footer_column_service_menu_after"}{/block}
                </ul>
            </nav>
        {/block}
    </div>
{/block}




{*
	
	<form class="newsletter--form" action="http://maelzer.schaeferpromotion1.timmeserver.de/newsletter" method="post">
<input type="hidden" value="1" name="subscribeToNewsletter">
<div class="content">
<input type="email" name="newsletter" class="newsletter--field" placeholder="Ihre E-Mail Adresse">
<input type="hidden" name="redirect">
<button type="submit" class="newsletter--button btn">
<i class="icon--mail"></i> <span class="button--text">Newsletter abonnieren</span>
</button>
</div>
<p class="privacy-information">
Ich habe die <a title="Datenschutzbestimmungen" href="http://maelzer.schaeferpromotion1.timmeserver.de/datenschutz" target="_blank">Datenschutzbestimmungen</a> zur Kenntnis genommen.
</p>
<input type="hidden" name="__csrf_token" value="Uf9b4O9sL4sDIdVozdmBx0LI9Z7niG"></form>
	
	
*}
