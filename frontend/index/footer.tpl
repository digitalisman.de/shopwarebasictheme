{extends file="parent:frontend/index/footer.tpl"}

{* Footer menu *}
{block name='frontend_index_footer_menu'}
    <div class="footer--columns block-group">
        {include file='frontend/index/footer-navigation.tpl'}
    </div>
{/block}

{* Logo *}
{*$category.attribute.categoriecolor|@var_dump*}
{*$sCategoryContent.attributes|@var_dump*}
{block name="frontend_index_shopware_footer_logo"}{/block}

