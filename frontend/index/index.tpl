{extends file="parent:frontend/index/index.tpl"}

{block name='frontend_index_after_body'}
	{$smarty.block.parent}
	{if $theme.show_holiday_banner}
		<section class="holiday_banner">
			<p>
				{*$theme.holiday_banner_text*}
				{s name='IndexHolidayBannerContent' namespace="frontend/index/holiday-banner"}Wir machen eine kurze Pause vom xxx bis xxx. Bitte beachten Sie, dass Ihre Bestellung in diesem Zeitraum nicht bearbeitet wird.{/s}
			</p>
		</section>
	{/if}
	
	{if $theme.show_socialmedia_banner}
		<section class="socialmedia_banner">
			<ul>
				<li>
					{s name='IndexSocialHeadline' namespace="frontend/index/holiday-banner"}folge uns{/s}
				</li>
				{if $theme.facebook_field_text}
					<li>
						<a href="{s name='IndexSocialFacebookURL' namespace="frontend/index/social-banner"}https://www.facebook.com/{/s}{$theme.facebook_field_text}" target="_blank">
							<img src="{link file='frontend/_public/src/img/icons/SocialMedia/'}facebook.svg" width="30" alt="facebook icon" title="facebook icon">
						</a>
					</li>
				{/if}
				{if $theme.instagram_field_text}
					<li>
						<a href="{s name='IndexSocialInstagramURL' namespace="frontend/index/social-banner"}https://www.instagram.com/{/s}{$theme.instagram_field_text}" target="_blank">
							<img src="{link file='frontend/_public/src/img/icons/SocialMedia/'}instagram.svg" width="30" alt="instagram icon" title="instagram icon">
						</a>
					</li>
				{/if}
				{if $theme.youtube_field_text}
					<li>
						<a href="{s name='IndexSocialYoutubeURL' namespace="frontend/index/social-banner"}https://www.youtube.com/user/{/s}{$theme.youtube_field_text}" target="_blank">
							<img src="{link file='frontend/_public/src/img/icons/SocialMedia/'}youtube.svg" width="30" alt="youtube icon" title="youtube icon">
						</a>
					</li>
				{/if}
			</ul>
		</section>
	{/if}
	
	{if isset($sCategoryContent.attribute.hiddenh1)}
		<h1 class="pagetitle">{$sCategoryContent.attribute.hiddenh1}</h1>
	{/if}
	
    {if $bestitAmazonPay}
        <div
            class="bestit-amazon-pay"
            data-async="{if $SwapAsync}1{else}0{/if}"
            data-controller="{$SwapController}"
            data-action="{$SwapAction}"
            data-sellerId="{$bestitAmazonPay.sellerId}"
            data-clientId="{$bestitAmazonPay.clientId}"
            data-purchaseId="{$bestitAmazonPay.purchaseId}"
            data-session="{$bestitAmazonPay.session}"
            data-smartphoneCollapsible="{$bestitAmazonPay.smartphoneCollapsible}">
        </div>
    {/if}
{/block}