{extends file="parent:frontend/listing/product-box/box-emotion.tpl"}

{* Product name *}
{block name='frontend_listing_box_article_name'}
	<h5>{$sArticle.supplierName|escape}</h5>
    <a href="{$sArticle.linkDetails}"
       class="product--title"
       title="{$productName|escapeHtml}">
        {$productName|truncate:50|escapeHtml}
    </a>
{/block}
