{extends file="parent:frontend/home/index.tpl"}

{*block name='frontend_index_content'*}
{*block name="frontend_index_footer"}
	
	<div class="footer--categories">
		<h2>
			{s namespace="frontend/home/index" name="CategorieListingHeadline"}
				Unser Sortiment
			{/s}
		</h2>
	
		<div class="catflex container">
			{foreach $sMainCategories as $sCategory}
			
				{if $sCategory.attribute.sortiment}
					<a href="{$sCategory.link}" class="flexitem" itemprop="url"{if $sCategory.external && $sCategory.externalTarget} target="{$sCategory.externalTarget}"{/if}>
						{if $sCategory.attribute.categoryimage}
							<img src="{$sCategory.attribute.categoryimage}" alt="{$sCategory.description}" width="380" height="280">
						{/if}
						<h3>{$sCategory.description}</h3>
					
					</a>
				{/if}
			{/foreach}
		</div>
	</div>
{/block*}