{extends file="parent:widgets/emotion/components/component_banner.tpl"}

{block name="widget_emotion_component_banner_image"}
	
	{$smarty.block.parent}

	{if isset($Data.title) }
		<div class="banner--title">
			<h2>{$Data.title|escape}</h2>
		</div>
	{/if}
{/block}

