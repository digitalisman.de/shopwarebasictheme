<?php

namespace Shopware\Themes\shopwarebasictheme;

use Shopware\Components\Form as Form;

class Theme extends \Shopware\Components\Theme
{
    protected $extend = 'Responsive';

    protected $name = <<<'SHOPWARE_EOD'
Basic Theme für Shopware
SHOPWARE_EOD;

    protected $description = <<<'SHOPWARE_EOD'

SHOPWARE_EOD;

    protected $author = <<<'SHOPWARE_EOD'
Daniel Vogel
SHOPWARE_EOD;

    protected $license = <<<'SHOPWARE_EOD'

SHOPWARE_EOD;

    public function createConfig(Form\Container\TabContainer $container)
	{
	    $fieldset = $this->createFieldSet(
	        'my_custom_settings',
	        'Einstellungen'
	    );
	    
	    // Create the textfield
		$textField = $this->createTextField(
		    'holiday_banner_text',
		    'Urlaubsmeldung Text',
		    ''
		);
		
		// Create the color picker field
		$colorPickerField = $this->createColorPickerField(
		    'holiday-color-main',
		    'Urlaubsmeldung Farbe',
		    '@brand-primary'
		);
		
		// Create Checkbox field
		$ShowholidayField = $this->createCheckboxField(
		    'show_holiday_banner',
		    'Urlaubsmeldung anzeigen',
		    false
		);
		
		// Create Checkbox field
		$ShowSocialMediaField = $this->createCheckboxField(
		    'show_socialmedia_banner',
		    'Social Media anzeigen',
		    false
		);
		
		// Create the textfield
		$textFieldFacebook = $this->createTextField(
		    'facebook_field_text',
		    'Facebook Profil URL',
		    ''
		);
		
		// Create the textfield
		$textFieldInstagram = $this->createTextField(
		    'instagram_field_text',
		    'Instagram Profil URL',
		    ''
		);
		
		// Create the textfield
		$textFieldYouTube = $this->createTextField(
		    'youtube_field_text',
		    'Youtube Profil URL',
		    ''
		);
		
		$fieldset->addElement($textField);
		$fieldset->addElement($colorPickerField);
		$fieldset->addElement($ShowholidayField);
		
		$fieldset->addElement($ShowSocialMediaField);
		$fieldset->addElement($textFieldFacebook);
		$fieldset->addElement($textFieldInstagram);
		$fieldset->addElement($textFieldYouTube);
	    
	    $tab = $this->createTab(
	        'my_custom_tab',
	        'Einstellungen'
	    );
	    
	    $tab->addElement($fieldset);
	    
	
	    $container->addTab($tab);
	}
}